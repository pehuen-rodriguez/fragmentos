/* 
  utiles.js
  funcionalidad: para sacar los nodos de texto y para formatear las fechas.
 */
var logger = require('./logger.js');
var https = require('https');

var utiles = {
    getTextNodesIn: function getTextNodesIn (node, includeWhitespaceNodes) {
        var textNodes = [], nonWhitespaceMatcher = /\S/;
        function getTextNodes(node) {
            if (node.nodeType == 3) {
                if (includeWhitespaceNodes || nonWhitespaceMatcher.test(node.nodeValue.replace(/(\r\n|\n|\r)/gm, ""))) {
                    textNodes.push(node);
                }
            // agrego consideración por los BR
            } else if (node.tagName == "BR") {
                textNodes.push("white_space");
            } else if (node.childNodes) {
                for (var i = 0, len = node.childNodes.length; i < len; ++i) {
                    getTextNodes(node.childNodes[i]);
                }
            }
        }
        getTextNodes(node);
        return textNodes;
    },
    getFormatedDate: function getFormatedDate (str) {
        var monthNames = [
          "enero", "febrero", "marzo",
          "abril", "mayo", "junio", "julio",
          "agosto", "septiembre", "octubre",
          "noviembre", "diciembre"
        ];

        var date = new Date(str);
        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return day + '-' + monthNames[monthIndex] + '-' + year;
    },
    loadWikipedia: function loadWikipedia(person, callback) {        
        var url = `https://es.wikipedia.org/wiki/${person.first}_${person.last}`;
        
        https.get(url, function(res) {
            var body = "";
            res.setEncoding("UTF-8");

            res.on("data", function(chunk) {
                body += chunk;
            });
            res.on("end", function() {
                callback(body);
            });
        });
    }
};

module.exports = utiles;