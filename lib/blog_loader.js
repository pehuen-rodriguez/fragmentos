/* jshint loopfunc: true */

/* 
	blog_loader.js
	funcionalidad: conecta a la API de blogger.
	por ahora, solamente loguea.
*/

var request = require("request");
var appConfig = require("../config/application.js"); //archivo de configuración que tiene el API key y la URL de base
var utiles = require("./utiles.js"); //formatea fechas y saca los nodos de text
var fs = require("fs");
var jsonfile = require('jsonfile');
var util = require('util');
var logger = require('./logger.js');
var jsdom = require("jsdom");

/* esto lo tengo que cachar de INPUT */
var URI_blog = 'http://fragmentosargentinos.blogspot.com.ar/';
/* --------------------------------- */
var URI_blogInfo = appConfig.URI_base + 'byurl?url=' + URI_blog + '&key=' + appConfig.API_KEY;

var nextPageToken;
//var parser = new DOMParser(); qué onda con node, no anda javascript
var doc = "";

request({
  url: URI_blogInfo,
  json: true
}, function (error, response, body) {
  if (!error && response.statusCode === 200) {
  	getPageData(body.posts.selfLink + '?key=' + appConfig.API_KEY);
  }
});

var getPageData = function (postsSelfLink, nextPageToken) {
	var URI_postsInfo = postsSelfLink + (typeof(nextPageToken) != "undefined" ? "&pageToken=" + nextPageToken : "");
	logger.info("URI_postsInfo: " + URI_postsInfo);
	var file = './tmp/' + (typeof(nextPageToken) != "undefined" ? nextPageToken : "primero") + ".json";

	request({
	  url: URI_postsInfo,
	  json: true
	}, function (error, response, body) {
	  if (!error && response.statusCode === 200) {
	  	nextPageToken = body.nextPageToken;
	  	//logger.info("typeof error: " + typeof(error));
	  	//logger.info("typeof response: " + typeof(response));
	  	//logger.info("typeof body: " + typeof(body));
	  	//logger.info('response: ' + JSON.stringify(response));
	  	
	  	for (var i = 0; i < body.items.length; i++) {
				//logger.info('fecha: ' + utiles.getFormatedDate(body.items[i].published));
				//logger.info('todo: ' + JSON.stringify(body.items[i]));
				var textNodes = [];				
				//doc va a ser el documento HTML
   			//doc = parser.parseFromString(body.items[i].content, "text/html");
   			//logger.info('content: ' + JSON.stringify(body.items[i].content));
   			var doc = jsdom.jsdom(body.items[i].content);
   			//logger.info('doc: ' + JSON.stringify(doc));
   			//var doc = jsdom.jsdom('<div style=margin-bottom: 0cm;>hola</div>');
   			// logger.info('doc: ' + JSON.stringify(doc));
   			// logger.info('doc: ' + doc.body.innerHTML);
   			// logger.info('doc: ' + doc);
   			textNodes = utiles.getTextNodesIn(doc.body);
				textNodes.forEach(function(textNode) {
					logger.info('textNode: ' + textNode.textContent);
				});
	  	}
	  	//si tengo una siguiente página, sigo
	  	if (typeof(nextPageToken) != "undefined") getPageData(postsSelfLink, nextPageToken);
	  }
	});
};