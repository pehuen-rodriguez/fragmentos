/* jshint loopfunc: true */
/* 
	TODO: cambiar por la conexión al blog
	fragmentosApp.js
	funcionalidad: obtiene mockup_input, que vino del blog
	adecúa los datos, y los lleva al formato que necesito para la web
 */

//
var data = require("./data/mockup_input"), //data lo uso?
	MongoClient = require('mongodb').MongoClient,
	assert = require('assert');
/* cambiar por la conexión */

var isSquare = function (n) {
	return n > 0 && Math.sqrt(n) % 1 === 0;
};

var closeConnection = function (db) {
	db.close();
	console.log("connection closed on tempo");
};

//si es cuadrado, lo dejo como está, sino le sumo 1
var dataLength = data.length;
var rows = Math.floor(Math.sqrt(dataLength)) + (isSquare(dataLength) ? 0 : 1);
var columns = Math.floor(dataLength / rows);

//inicializo la matrix
var matrix = [];
for (var i = 0; i < rows; i++) {
	matrix[i] = [];
}

//logueo una referencia
//console.log("totales: " +  rows + " | " + columns);

//lleno la matrix
var rowsCount = 0;
var columnsCount = 0;

//después recorrerla y actualizarla 
//para crear las referencias de cada objecto
//para hacerlo, es cuando la meto adentro de la matrix
MongoClient.connect('mongodb://localhost:27017/fragmentos', function(err, db) {

	assert.equal(null, err);

	console.log('connected to server');
	var collection = db.collection("fragmentos");
	collection.insert(data);

	//después de insertarlos, los obtengo y lleno la matrix, para armar las referencias
	db.collection('fragmentos').find({}).toArray(function(err, docs) {

		assert.equal(null, err);

		docs.forEach(function (doc) {
			
			//console.log(JSON.stringify(doc));
			matrix[rowsCount][columnsCount] = doc;

			columnsCount++;
			if (columnsCount > columns) {
				columnsCount = 0;
				rowsCount++;
			}

		});

		//referencias
		var above, right, below, left;

		var callbackCount = 0;
		//ahora, actualizo cada document, con un document de referencias
		for (var i = 0; i < matrix.length; i++) { // filas
			for (var j = 0; j < matrix[i].length; j++) { // columnas
				
				//la primer fila no tiene arriba
				if (i === 0) { above = false; } else {
					above = typeof(matrix[i-1][j]) != "undefined" && matrix[i-1][j]._id;
				}
				//la última fila no tiene abajo
				if (i === matrix.length - 1) { below = false; } else {
					below = typeof(matrix[i+1][j]) != "undefined" && matrix[i+1][j]._id;
				}
				//la primer columna no tiene izquierda
				if (j === 0) { left = false; } else {
					left = typeof(matrix[i][j-1]) != "undefined" && matrix[i][j-1]._id;
				}
				//la última columna no tiene derecha
				if (j === matrix[i].length - 1) { right = false; } else {
					right = typeof(matrix[i][j+1]) != "undefined" && matrix[i][j+1]._id;
				}
				references = {
					"above": above,
					"right": right,
					"below": below,
					"left": left
				};

				//console.log(matrix[i][j]._id);
				db.collection("fragmentos").updateOne(
					{ "_id": matrix[i][j]._id },
					{ $set: { "references": references } },					
					function (err, results) {
						assert.equal(null, err);							
						if(++callbackCount >= docs.length) {
							closeConnection(db);
						}
				});	

			}
		}
	});
});
