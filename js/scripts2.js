// https://www.googleapis.com/blogger/v3/blogs/35068669 self link de fragmentosargentinos
// AIzaSyAKXPP5xfNBjA_ZnJsIT5HdZhT7V0AqI90 mi API key

var API_KEY = 'AIzaSyAKXPP5xfNBjA_ZnJsIT5HdZhT7V0AqI90';
var BlogSelfId = '35068669';
var BlogInfoUri = 'https://www.googleapis.com/blogger/v3/blogs/byurl?url=http://fragmentosargentinos.blogspot.com.ar/&key=AIzaSyAKXPP5xfNBjA_ZnJsIT5HdZhT7V0AqI90';
var BlogPostsUri = 'https://www.googleapis.com/blogger/v3/blogs/' + BlogSelfId + '/posts?key=' + API_KEY;

function getFormatedDate (str) {
    var monthNames = [
      "Enero", "Febrero", "Marzo",
      "Abril", "Mayo", "Junio", "Julio",
      "Agosto", "Septiembre", "Octubre",
      "Noviembre", "Diciembre"
    ];

    var date = new Date(str);
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + '-' + monthNames[monthIndex] + '-' + year;
}

function getTextNodesIn(node, includeWhitespaceNodes) {
    var textNodes = [], nonWhitespaceMatcher = /\S/;

    function getTextNodes(node) {
        if (node.nodeType == 3) {
            if (includeWhitespaceNodes || nonWhitespaceMatcher.test(node.nodeValue.replace(/(\r\n|\n|\r)/gm, ""))) {
                textNodes.push(node);
            }
        // agrego consideración por los BR
        } else if (node.tagName == "BR") {
            textNodes.push(document.createTextNode("white_space"));
        } else {
            for (var i = 0, len = node.childNodes.length; i < len; ++i) {
                getTextNodes(node.childNodes[i]);
            }
        }
    }

    getTextNodes(node);
    return textNodes;
}

function getPageData(page) {

    $.getJSON(page)

    .done(function (data) {

        nextPageToken = data.nextPageToken;

        //recorro los posts: data.items
        for (var h = 0; h < data.items.length; h++) {

            //convierto el contenido del post a un elemento jquery
            $postHtml = $(data.items[h].content);
            
            //tiro las características del post
            //pero creando un ul primero, para tener cada post diferenciado
            
            $('<ul>', { id: "post" + h }).appendTo($('#contenedor'));
            
            $('<li>', { text: "indice: " + h }).appendTo($("#post" + h)).hide();
            $('<li>', { text: "título: " + data.items[h].title }).appendTo($("#post" + h));
            $('<li>', { text: "fecha formateada : " + getFormatedDate(data.items[h].published) }).appendTo($("#post" + h));
            $('<li>', { text: "fecha : " + data.items[h].published }).appendTo($("#post" + h));

            $('<ul>', { id: "post_content" + h }).appendTo($("#post" + h));

            var escribir2 = "";

            //cada elemento dentro del post
            $.each($postHtml, function (i, item) {

                if (item.tagName != "STYLE") {
                    texto = getTextNodesIn(item);

                    //si tiene texto, me interesa
                    if (texto.length) {
                        var escribir_buffer = "";
                        for (var j = 0; j < texto.length; j++) {
                            if (texto[j].textContent === "white_space") {
                                //si hay un espacio en blanco escribo lo que tengo y vacio el buffer
                                $('<li>', { text: "subindice: " + i }).appendTo($("#post_content" + h)).hide();
                                $('<li>', { text: "tagName: " + item.tagName }).appendTo($("#post_content" + h)).hide();
                                $('<li>', { text: /*"texto: " +*/ escribir_buffer }).appendTo($("#post_content" + h));
                                escribir_buffer = "";
                            } else {
                                escribir_buffer += texto[j].textContent;
                            }
                        }

                        if (escribir_buffer) {                        
                            $('<li>', { text: "subindice: " + i }).appendTo($("#post_content" + h)).hide();
                            $('<li>', { text: "tagName: " + item.tagName }).appendTo($("#post_content" + h)).hide();
                            $('<li>', { text: /*"texto: " +*/ escribir_buffer }).appendTo($("#post_content" + h));
                        }
                    }
                    else {
                        $('<li>', { text: "subindice: " + i }).appendTo($("#post_content" + h)).hide();
                        $('<li>', { text: "tagName: " + item.tagName }).appendTo($("#post_content" + h)).hide();
                    }
                }
            });            
        }
    });

    if (nextPageToken) {
        nextPage = BlogPostsUri + "&pageToken=" + nextPageToken;
        //getPageData(nextPage);
    }
}

$(document).ready(function () {

    var nextPageToken = "";
    getPageData(BlogPostsUri);

});