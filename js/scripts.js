﻿// https://www.googleapis.com/blogger/v3/blogs/35068669 self link de fragmentosargentinos
// AIzaSyAKXPP5xfNBjA_ZnJsIT5HdZhT7V0AqI90 mi API key

var API_KEY = 'AIzaSyAKXPP5xfNBjA_ZnJsIT5HdZhT7V0AqI90';
var BlogSelfId = '35068669';
var BlogInfoUri = 'https://www.googleapis.com/blogger/v3/blogs/byurl?url=http://fragmentosargentinos.blogspot.com.ar/&key=AIzaSyAKXPP5xfNBjA_ZnJsIT5HdZhT7V0AqI90';
var BlogPostsUri = 'https://www.googleapis.com/blogger/v3/blogs/' + BlogSelfId + '/posts?key=' + API_KEY;

function getTextNodesIn(node, includeWhitespaceNodes) {
    var textNodes = [], nonWhitespaceMatcher = /\S/;

    function getTextNodes(node) {
        textNodes.push(node);
        if (node.nodeType == 3) {
            if (includeWhitespaceNodes || nonWhitespaceMatcher.test(node.nodeValue.replace(/(\r\n|\n|\r)/gm, ""))) {
                textNodes.push(node);
            }
        } else {
            for (var i = 0, len = node.childNodes.length; i < len; ++i) {
                getTextNodes(node.childNodes[i]);
            }
        }
    }

    getTextNodes(node);
    return textNodes;
}

function getPageData(page) {

    //obtengo toda la info de la página
    $.getJSON(page).done(function (data) {
        
        //recorro los posts: data.items
        for (var h = 0; h < data.items.length; h++) {

            //convierto el contenido del post a un elemento jquery
            $postHtml = $(data.items[h].content);
            
            //cada elemento dentro del post
            $.each($postHtml, function (i, item) {

                //solo excluyo a priori los de tipo STYLE
                if (item.tagName != "STYLE") {

                    //saco el texto de los nodos
                    texto = getTextNodesIn(item, true);
                    if (texto.length) {
                        var escribir = "";
                        for (var j = 0; j < texto.length; j++) {
                            escribir += texto[j].textContent;
                        }

                        escribir2 += escribir + " - ";
                    }
                }
            });

            $('<li>', { text: "index: " +  h + " - title: " + data.items[h].title + " - fecha: " + data.items[h].published + " - texto: " + escribir2 }).appendTo($('.postsList'));
        }

        nextPageToken = data.nextPageToken;

        if (nextPageToken) {

            nextPage = BlogPostsUri + "&pageToken=" + nextPageToken;
            //getPageData(nextPage);

        }

    });
}


$(document).ready(function () {

    var nextPageToken = "";
    getPageData(BlogPostsUri);

});