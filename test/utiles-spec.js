var expect = require("chai").expect;
var tools = require("../lib/utiles");
var nock = require("nock");

describe("utiles", function() {

	describe("loadWikipedia()", function() {

		before(function () {
			nock("https://es.wikipedia.org")
				.get("/wiki/Víctor_Sueiro")
				.reply(200, "Mock Víctor Sueiro page");
		});
		it("should load the Victor Sueiro Wikipedia page", function(done) {

			tools.loadWikipedia({ first: "Víctor", last: "Sueiro" }, function(html) {
          expect(html).to.equal("Mock Víctor Sueiro page");
          done();
      });

		});
	});

	describe("getFormatedDate()", function() {
		it("should print the date formatted", function() {

			var results = tools.getFormatedDate("2015-11-01T14:45:00Z");
			expect(results).to.equal("1-noviembre-2015");

		});
	});
});
