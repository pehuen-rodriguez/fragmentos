/* jshint laxcomma: true, esnext: true */
// Module dependencies
var express = require('express'), 
	stylus = require('stylus'), 
	nib = require('nib'), 
	morgan = require('morgan'), 
	MongoClient = require('mongodb').MongoClient, 
	cons = require('consolidate'), 
	assert = require('assert');

var ObjectId = require('mongodb').ObjectId;

//la app va a ser express
var app = express();

//esto no sé qué mierda es. Creo que compila el styl
//y nib? una incógnita
function compile(str, path) {
	return stylus(str)
		.set('filename', path)
		.use(nib());
}

function randomIntInc (low, high) {
	return Math.floor(Math.random() * (high - low + 1) + low);
}

//seteo el engine que quiero usar
app.engine('html', cons.jade);
app.set('view engine', 'jade');

//y seteo el directorio para las views
app.set('views', __dirname + '/views');

//morgan que no se para que mierda es
app.use(morgan('combined'));

//y que los css van a estar en stylus
app.use(stylus.middleware(
	{
		src: __dirname + '/public', 
		compile: compile
	}
));
//express static que tampoco sé para qué es, pero probablemente para poder usar
//los css
app.use(express.static(__dirname + '/public'));

MongoClient.connect('mongodb://localhost:27017/fragmentos', function(err, db) {

	assert.equal(null, err);
	console.log('connected to server');

	db.collection('fragmentos').find({}).toArray(function(err, docs) {

		var startIndex = docs[randomIntInc(0, docs.length-1)]._id;

		//funcion que atrapa los req en el "/" o raiz
		app.get('/', function (req, res) {

			//en req.query están las variables GET
			if (typeof(req.query.id) !== 'undefined') {
				startIndex = ObjectId(req.query.id);
			}

			db.collection('fragmentos').findOne({ "_id": startIndex }, function (err, doc) {

				assert.equal(null, err);
			  res.render('index', { title : 'fragmento', "doc" : doc });

			});

		});

		//inicialización del server junto con un callback para mostrar el listening
		var server = app.listen(3000, function() {
			var port = server.address().port;
			console.log(`Express server listening on port ${port}`);
		});
		
	});

});